const mongoose = require('mongoose');

const Note = mongoose.model('notes', {
  text: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false
  }
});

module.exports = {
  Note
};