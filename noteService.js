const { Note } = require('./Note.js');

const createNote = (req, res, next) => {
  try {
  const note = new Note({
    text: req.body.text,
    userId: req.user.userId
  });

  note.save().then((saved) => {
    res.json({message: 'created successfully'});
  });
  } catch (error) {
    res.status(404).json('Server error createNote')
  }
}

const getAllNotes = (req, res, next) => {
  try {
  Note.find({userId: req.user.userId}).then((result) => {
    res.json({notes: result});
  });
  } catch (error) {
    res.status(404).json('Server error getAllNote')
  }
}

const getSingleNote = (req, res, next) => Note.findById(req.params.id)
.then((note) => {
  res.json({note: note});
});

const updateNote = (req, res, next) => {
  const { text } = req.body;
  return Note.findByIdAndUpdate(req.params.id, {$set: { text } })
    .then((note) => {
      res.json({message: 'updated'});
    });
};

const deleteNote = (req, res, next) => Note.findByIdAndDelete(req.params.id)
  .then((note) => {
    res.json({message: 'deleted note'});
  });

const checkNote = (req, res, next) => {
  return Note.findByIdAndUpdate(req.params.id, {$set: { completed: true } })
    .then((note) => {
      res.json({message: 'checked note'});
    });
}

module.exports = {
  createNote,
  getAllNotes,
  getSingleNote,
  updateNote,
  deleteNote,
  checkNote
};