const { Note } = require('./Note.js');
const express = require('express');
const router = express.Router();
const { createNote, getAllNotes, getSingleNote, updateNote, deleteNote, checkNote } = require('./noteService.js');
const { authMiddleware } = require('./authMiddleware');


router.post('/',authMiddleware, createNote)

router.get('/',authMiddleware, getAllNotes)

router.get('/:id', getSingleNote)

router.put('/:id', updateNote)

router.patch('/:id', checkNote)

router.delete('/:id', deleteNote)

module.exports = {
  notesRouter: router,
};