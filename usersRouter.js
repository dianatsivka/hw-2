const express = require('express');
const { User } = require('./User.js');
const router = express.Router();
const { registerUser, loginUser, changePassword, getProfileInfo, deleteProfile } = require('./usersService.js');
const { authMiddleware } = require('./authMiddleware');


router.post('/auth/register', registerUser)

router.post('/auth/login', loginUser)

router.patch('/users/me', authMiddleware, changePassword)

router.get('/users/me', authMiddleware, getProfileInfo)

router.delete('/users/me', authMiddleware, deleteProfile)

module.exports = {
  usersRouter: router,
};